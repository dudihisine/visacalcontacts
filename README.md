# README #

### What is this repository for? ###

This app is a job interview project

### How do I get set up? ###

Clone the repo, Run 'pod install'
Open the '.xcworkspace' file and run on iOS simulator or iOS device.
*the project was built on Xcode 12, there can be some issues running on older Xcodes.

### App Design pattern & features ###

The app design pattern is MVVM.
The app using 'CoreStore' saving and fetching data from CoreData.
The app using 'Combine' for viewModel and view binding.
The app using 'PhoneNumberKit' for handling phone number input.

### Screens ###

#### VCCContactListVC ####

Contains a list of stored contacts and a button to add new contact.

#### VCCManageContact ####

This screen has 2 modes

##### Edit #####
User can insert information regardean the contact.
*also used for adding new contact.

##### View #####
User can view information regardean the contact and make actions like call, send email, send SMS or mark the contact as favorite.

### Contact ###

* Full name: Dudi Hisine
* Email : dudihisine@gmail.com
