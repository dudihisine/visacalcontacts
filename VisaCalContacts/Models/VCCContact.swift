//
//  VCC_Contact.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 24/12/2020.
//

import UIKit
import CoreData

//MARK:Contact Base Protocol
protocol VCCContactProtocol : AnyObject{

    //MARK:Variables
    var name: String? {get set}
    var familyName: String? {get set}
    var email: String? {get set}
    var phoneNumber: String? {get set}
    var favorite: NSNumber! {get set}
    var isFavorite: Bool{get set}
    var imageData: Data? {get set}
}

//MARK:Contact Base Extension
extension VCCContactProtocol{
    var isFavorite: Bool{
        get {
            return Bool(truncating: favorite)
        }
        set {
            favorite = NSNumber(value: newValue)
        }
    }
}

//MARK:Contact(Not Menaged Object)
class VCCContactRep: NSObject, VCCContactProtocol{

    //MARK:Variables
    var name: String?
    var familyName: String?
    var email: String?
    var phoneNumber: String?
    var favorite: NSNumber!
    var imageData: Data?

    //MARK:Constructor
    init(with name: String?, familyName: String?, email: String?, isFavorite: Bool, phoneNumber: String?, image: UIImage?) {
        super.init()
        self.name = name
        self.familyName = familyName
        self.email = email
        self.isFavorite = isFavorite
        self.phoneNumber = phoneNumber
        self.imageData = image?.jpegData(compressionQuality: 0.7)
    }
}

//MARK:Contact(Menaged Object)
final class VCCContact: NSManagedObject, VCCContactProtocol{

    //MARK:Variables
    @NSManaged
    dynamic var name: String?
    @NSManaged
    dynamic var familyName: String?
    @NSManaged
    dynamic var email: String?
    @NSManaged
    dynamic var phoneNumber: String?
    @NSManaged
    dynamic var favorite: NSNumber!
    @NSManaged
    dynamic var imageData: Data?

    //MARK:Help Functions
    func getFullName() ->String{

        var fullName = ""

        if let name = name{
            fullName = "\(name)"
        }

        if let familyName = familyName{
            fullName = fullName.isEmpty ? familyName : "\(fullName) \(familyName)"
        }

        return fullName
    }

    func getImage() -> UIImage?{
        if let imageData = imageData{
            return UIImage(data: imageData)
        }

        return nil
    }
}


