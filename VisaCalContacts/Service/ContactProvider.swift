//
//  ContactProvider.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 24/12/2020.
//

import UIKit
import CoreStore

class ContactsProvider: ListObjectDataProvider{

    //MARK:Typealias
    typealias ObjectType = VCCContact
    typealias ObjectRepType = VCCContactRep

    //MARK:Variables
    let dataStack: DataStack = {
        let dataStack = DataStack(xcodeModelName: "VisaCalContacts")

        do{
            try dataStack.addStorageAndWait(
                SQLiteStore(
                    fileName: "VisaCalContacts.sqlite",
                    localStorageOptions: .recreateStoreOnModelMismatch
                )
            )
        }catch{
            print("DataStoreHelper - \(error)")
        }

        return dataStack
    }()

    //MARK:Manage Functions
    func saveObject(_ object: ObjectRepType) {
        dataStack.perform { (transaction) in
            let storedContact = transaction.create(Into<ObjectType>())

            storedContact.name = object.name
            storedContact.familyName = object.familyName
            storedContact.email = object.email
            storedContact.isFavorite = object.isFavorite
            storedContact.phoneNumber = object.phoneNumber
            storedContact.imageData = object.imageData
        } completion: { (result) -> Void in
            switch result {
            case .success: print("success!")
            case .failure(let error): print(error)
            }
        }
    }

    func removeObject(_ object: ObjectType) {
        dataStack.perform { (transaction) in
            transaction.delete([object])
        } completion: { (result) -> Void in
            switch result {
            case .success: print("success!")
            case .failure(let error): print(error)
            }
        }
    }

    func updateObject(object: ObjectType, with newObject: ObjectRepType) {
        dataStack.perform { (transaction) in
            if let storedContact = transaction.edit(object){
                storedContact.name = newObject.name
                storedContact.familyName = newObject.familyName
                storedContact.email = newObject.email
                storedContact.isFavorite = newObject.isFavorite
                storedContact.phoneNumber = newObject.phoneNumber
                storedContact.imageData = newObject.imageData
            }
        } completion: { (result) -> Void in
            switch result {
            case .success: print("success!")
            case .failure(let error): print(error)
            }
        }
    }

    func getMonitor(orderBy key: String) -> ListMonitor<ObjectType> {
        return dataStack.monitorList(From<ObjectType>() .orderBy(.ascending(key)))
    }
}
