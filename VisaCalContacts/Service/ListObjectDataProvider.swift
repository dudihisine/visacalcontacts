//
//  ListObjectDataProvider.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 25/12/2020.
//

import UIKit
import CoreStore

protocol ListObjectDataProvider{

    //MARK:Associatedtypes
    associatedtype ObjectType : DynamicObject
    associatedtype ObjectRepType : NSObject

    //MARK:Manage Functions
    func saveObject(_ object : ObjectRepType)
    func removeObject(_ object: ObjectType)
    func updateObject(object: ObjectType, with newObject: ObjectRepType)
    func getMonitor(orderBy key: String) -> ListMonitor<ObjectType>
}
