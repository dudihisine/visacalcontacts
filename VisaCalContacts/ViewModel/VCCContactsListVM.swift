//
//  VCCContactsListVM.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 24/12/2020.
//

import Foundation
import CoreStore

class VCCContactsListVM<Cell: UITableViewCell> : NSObject, UITableViewDataSource{

    //MARK:Variables
    @Published var monitor: ListMonitor<ContactsProvider.ObjectType>!
    private var cellIdentifier: String!
    public var configureCell: ((Cell,ContactsProvider.ObjectType) -> ())!
    var contactsProvider : ContactsProvider!

    //MARK:Constructor
    init(contactsProvider: ContactsProvider, orderBy key: String, cellIdentifier :String, configureCell: @escaping (Cell,ContactsProvider.ObjectType) -> ()) {
        super.init()
        self.contactsProvider = contactsProvider
        self.cellIdentifier = cellIdentifier
        self.configureCell = configureCell
        let monitor = contactsProvider.getMonitor(orderBy: key)
        monitor.addObserver(self)
        self.monitor = monitor
    }

    //MARK:Help Functions
    public func getManageContactVC(with loadedContact: ContactsProvider.ObjectType? = nil) -> VCCManageContactVC{
        return VCCManageContactVC(provider: contactsProvider, with: loadedContact)
    }

    //MARK:TableView Datasource Delegate Methods
    public func numberOfSections(in tableView: UITableView) -> Int {
        return monitor.numberOfSections()
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return monitor.numberOfObjects(in: section)
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as! Cell
        let item = monitor[indexPath.row]
        self.configureCell(cell,item)
        return cell
    }

    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            contactsProvider.removeObject(monitor[indexPath.row])
        }
    }
}

extension VCCContactsListVM: ListObserver{

    // MARK: ListObserver
    func listMonitorDidChange(_ monitor: ListMonitor<ContactsProvider.ObjectType>) {
        self.monitor = monitor
    }

    func listMonitorDidRefetch(_ monitor: ListMonitor<ContactsProvider.ObjectType>) {
        self.monitor = monitor
    }
}

