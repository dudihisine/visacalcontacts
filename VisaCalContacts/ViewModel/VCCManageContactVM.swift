//
//  VCCManageContactVM.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 26/12/2020.
//

import UIKit

//MARK:Manage Done Action
enum VCCManageDoneAction{
    case save
    case saveExit
    case exit
}

//MARK:Manage State
enum VCCManageState{
    case edit
    case view

    //MARK:Help Functions
    func getActionBtnTitle() ->String{
        switch self{
        case .edit:
            return "Save"
        case .view:
            return "Close"
        }
    }

    func getOppesite() ->VCCManageState{
        switch self{
        case .edit:
            return .view
        case .view:
            return .edit
        }
    }

    func getBoolValue() ->Bool{
        switch self{
        case .edit:
            return true
        case .view:
            return false
        }
    }

    func getBorderStyle() ->UITextField.BorderStyle{
        switch self{
        case .edit:
            return .roundedRect
        case .view:
            return .none
        }
    }

    func getTextFieldHidden(isEmpty: Bool)-> Bool{
        switch self{
        case .edit:
            return false
        case .view:
            return isEmpty
        }
    }
}

class VCCManageContactVM: NSObject {

    //MARK:Variables
    private var contactsProvider: ContactsProvider!
    @Published var loadedContact: ContactsProvider.ObjectType?{
        didSet{
            state = loadedContact == nil ? .edit : .view
        }
    }
    @Published var state: VCCManageState = .view

    //MARK:Constructor
    init(contactsProvider: ContactsProvider, with loadedContact: ContactsProvider.ObjectType? = nil) {
        super.init()
        self.contactsProvider = contactsProvider
        self.loadedContact = loadedContact
    }

    //MARK:Help Functions
    func didSaveContact(with name: String?, familyName: String?, email: String?, isFavorite: Bool, phoneNumber: String?, image: UIImage?){
        let contact = ContactsProvider.ObjectRepType(with: name, familyName: familyName, email: email, isFavorite: isFavorite, phoneNumber: phoneNumber, image: image)

        if let loadedContact = loadedContact{
            contactsProvider.updateObject(object: loadedContact, with: contact)
        }else{
            contactsProvider.saveObject(contact)
        }
    }

    func switchState(){
        state = state.getOppesite()
    }

    func getDoneAction() ->VCCManageDoneAction{
        if loadedContact != nil{
            if state == .edit{
                return .save
            }else{
                return .exit
            }
        }

        return .saveExit
    }
}
