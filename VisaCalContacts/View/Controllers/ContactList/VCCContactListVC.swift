//
//  ViewController.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 24/12/2020.
//

import UIKit
import CoreStore
import Combine

class VCCContactListVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var tbvContacts: UITableView!{
        didSet{
            tbvContacts.register(UINib(nibName: VCCContactCell.identifier, bundle: nil), forCellReuseIdentifier: VCCContactCell.identifier)
            tbvContacts.delegate = self
            tbvContacts.dataSource = viewModel
        }
    }

    //MARK:Variables
    private var viewModel: VCCContactsListVM<VCCContactCell>!
    private var cancellables = Set<AnyCancellable>()
    override var navigationItem: UINavigationItem{
        return UINavigationItem(title: "Contacts")
    }

    //MARK:Lifecycle
    init(provider: ContactsProvider) {
        super.init(nibName: String(describing: VCCContactListVC.self), bundle: nil)
        viewModel = VCCContactsListVM(contactsProvider: provider, orderBy: "name", cellIdentifier: VCCContactCell.identifier){ cell, contact in
            cell.update(contact)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.prefersLargeTitles = true

        bindViewModel()
    }

    //MARK:Bind
    func bindViewModel(){
        viewModel?.$monitor.sink(receiveValue: { [weak self] monitor in
            guard let self = self else{ return }
            
            self.tbvContacts.reloadData()
        }).store(in: &cancellables)
    }

    //MARK:Button Actions
    @IBAction func addNewContactPressed(_ sender: Any) {
        present(viewModel.getManageContactVC(), animated: true)
    }
}

extension VCCContactListVC: UITableViewDelegate{

    //MARK:UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        present(viewModel.getManageContactVC(with: viewModel.monitor[indexPath]), animated: true)
    }
}

