//
//  VCCManageContactVC.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 24/12/2020.
//

import UIKit
import Combine
import MessageUI
import PhoneNumberKit

class VCCManageContactVC: UIViewController {

    //MARK:Outlets
    @IBOutlet weak var btnContactImage: UIButton!{
        didSet{
            btnContactImage.makeRound(with: 15)
            btnContactImage.imageView?.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var svActions: UIStackView!
    @IBOutlet weak var btnCall: UIButton!{
        didSet{
            btnCall.makeRound(with: 15)
        }
    }
    @IBOutlet weak var btnMail: UIButton!{
        didSet{
            btnMail.makeRound(with: 15)
            btnMail.isHidden = !MFMailComposeViewController.canSendMail()
        }
    }
    @IBOutlet weak var btnSMS: UIButton!{
        didSet{
            btnSMS.makeRound(with: 15)
            btnSMS.isHidden = !MFMessageComposeViewController.canSendText()
        }
    }
    @IBOutlet weak var btnFavorite: UIButton!{
        didSet{
            btnFavorite.makeRound(with: 15)
        }
    }
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfFamily: UITextField!
    @IBOutlet weak var tfNumber: PhoneNumberTextField!{
        didSet{
            tfNumber.withFlag = true
            tfNumber.withExamplePlaceholder = true
        }
    }
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnMainAction: UIButton!

    //MARK:Variables
    private var viewModel: VCCManageContactVM!
    private var cancellables = Set<AnyCancellable>()

    //MARK:Lifecycle
    init(provider: ContactsProvider, with loadedContact: ContactsProvider.ObjectType? = nil) {
        super.init(nibName: String(describing: VCCManageContactVC.self), bundle: nil)
        viewModel = VCCManageContactVM(contactsProvider: provider, with: loadedContact)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround() 
        bindViewModel()
    }

    //MARK:Bind
    func bindViewModel(){
        viewModel.$loadedContact.sink(receiveValue: { [weak self] loadedContact in
            guard let self = self, let loadedContact = loadedContact else{ return }
            self.loadContact(loadedContact: loadedContact)
        }).store(in: &cancellables)

        viewModel.$state.sink { [weak self] (state) in
            guard let self = self else{ return }
            self.changeState(state: state)
        }.store(in: &cancellables)
    }

    //MARK:Help Functions
    func loadContact(loadedContact: ContactsProvider.ObjectType){
        self.tfName.text = loadedContact.name
        self.tfFamily.text = loadedContact.familyName
        self.tfNumber.text = loadedContact.phoneNumber
        self.tfEmail.text = loadedContact.email
        self.btnFavorite.isSelected = loadedContact.isFavorite
        self.btnContactImage.setImage(loadedContact.getImage(), for: .normal)
    }

    func changeState(state: VCCManageState){
        tfName.setManageState(state: state)
        tfFamily.setManageState(state: state)
        tfNumber.setManageState(state: state)
        tfEmail.setManageState(state: state)

        svActions.isHidden = state.getBoolValue()
        btnEdit.isHidden = state.getBoolValue()
        
        btnMainAction.setTitle(state.getActionBtnTitle(), for: .normal)
    }
}

extension VCCManageContactVC{

    //MARK:Button Actions
    @IBAction func contactImageBtnPressed(_ sender: Any) {
        let controller = Generator.createImagePicker()
        controller.delegate = self
        present(controller, animated: true)
    }

    @IBAction func editBtnPressed(_ sender: Any) {
        viewModel.switchState()
    }

    @IBAction func mainActionBtnPressed(_ sender: Any) {
        if viewModel.getDoneAction() == .save || viewModel.getDoneAction() == .saveExit{
            let name = tfName.text
            let familyName = tfFamily.text
            let email = tfEmail.text
            let number = tfNumber.text
            let favorite = btnFavorite.isSelected
            let image = btnContactImage.image(for: .normal)

            viewModel.didSaveContact(with: name, familyName: familyName, email: email, isFavorite: favorite, phoneNumber: number, image: image)
        }

        if viewModel.getDoneAction() == .exit || viewModel.getDoneAction() == .saveExit{
            dismiss(animated: true)
        }else{
            viewModel.switchState()
        }
    }

    @IBAction func callBtnPressed(_ sender: Any) {
        guard let value = tfNumber.text, let phoneNumber = Generator.createValidPhone(from: value) , let number = URL(string: "tel://" + phoneNumber) else { return }
        UIApplication.shared.open(number)
    }

    @IBAction func mailBtnPressed(_ sender: Any) {
        guard let mail = tfEmail.text, let controller = Generator.createEmailMessage(mail: mail) else { return }
        controller.mailComposeDelegate = self
        present(controller, animated: true)
    }

    @IBAction func smsBtnPressed(_ sender: Any) {
        guard let value = tfNumber.text, let phoneNumber = Generator.createValidPhone(from: value), let controller = Generator.createSMSMessage(phoneNumber: phoneNumber) else { return }
        controller.messageComposeDelegate = self
        present(controller, animated: true, completion: nil)
    }

    @IBAction func favoriteBtnPressed(_ sender: Any) {
        btnFavorite.isSelected = !btnFavorite.isSelected
    }
}

extension VCCManageContactVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate{

    //MARK:UINavigationControllerDelegate & UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let tempImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            btnContactImage.setImage(tempImage, for: .normal)
        }
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension VCCManageContactVC: MFMessageComposeViewControllerDelegate{

    //MARK:MFMessageComposeViewControllerDelegate
    func messageComposeViewController(_ controller:MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension VCCManageContactVC: MFMailComposeViewControllerDelegate{

    //MARK:MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
