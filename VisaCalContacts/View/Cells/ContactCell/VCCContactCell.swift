//
//  VCCContactCell.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 26/12/2020.
//

import UIKit

class VCCContactCell: UITableViewCell{

    //MARK:Static variables
    static let identifier : String = String(describing: VCCContactCell.self)

    //MARK:Outlets
    @IBOutlet weak var lblContactFullName: UILabel!
    @IBOutlet weak var imvContactPicture: UIImageView!{
        didSet{
            imvContactPicture.makeRound(with: 15)
        }
    }

    //MARK:Variables
    override var reuseIdentifier: String?{
        return VCCContactCell.identifier
    }

    //MARK:Help Functions
    func update(_ contact: ContactsProvider.ObjectType){
        lblContactFullName.text = contact.getFullName()
        imvContactPicture.image = contact.imageData == nil ? UIImage(systemName: "person.fill") : contact.getImage()
    }
}
