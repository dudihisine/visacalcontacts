//
//  UITextFieldExtension.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 27/12/2020.
//

import UIKit

extension UITextField{

    //MARK: VCCManageState Support
    func setManageState(state: VCCManageState){
        isUserInteractionEnabled = state.getBoolValue()
        isHidden = state.getTextFieldHidden(isEmpty: text?.isEmpty ?? true)
        borderStyle = state.getBorderStyle()
    }
}
