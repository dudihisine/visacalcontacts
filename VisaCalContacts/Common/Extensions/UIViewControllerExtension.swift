//
//  UIViewControllerExtension.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 26/12/2020.
//

import UIKit

extension UIViewController {

    //MARK: Remove keyboard on tap
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
