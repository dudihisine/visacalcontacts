//
//  UIViewExtension.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 26/12/2020.
//

import UIKit

extension UIView{
    
    //MARK: Round view
    func makeRound(with cornerRadius: CGFloat){
        self.layer.masksToBounds = false
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
    }
}
