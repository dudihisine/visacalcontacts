//
//  Generator.swift
//  VisaCalContacts
//
//  Created by Dudi Hisine on 26/12/2020.
//

import UIKit
import MessageUI
import PhoneNumberKit

enum Generator{

    //MARK: Phone
    static func createValidPhone(from value: String) ->String?{
        let phoneNumberKit = PhoneNumberKit()

        do {
            let phoneNumber = try phoneNumberKit.parse(value)
            return phoneNumberKit.format(phoneNumber, toType: .e164)
        }
        catch {
            print("Generic parser error")
            return nil
        }
    }

    //MARK: Email Message
    static func createEmailMessage(mail: String) ->MFMailComposeViewController?{
        if MFMailComposeViewController.canSendMail(), Validator.isValidEmail(value: mail) {
            let controller = MFMailComposeViewController()
            controller.setToRecipients([mail])
            return controller
        }

        print("Cannot send email")
        return nil
    }

    //MARK: SMS Message
    static func createSMSMessage(phoneNumber: String) ->MFMessageComposeViewController?{
        if MFMessageComposeViewController.canSendText(){
            let controller = MFMessageComposeViewController()
            controller.recipients = [phoneNumber] //Here goes whom you wants to send the message
            return controller
        }
        print("Cannot send the message")
        return nil
    }

    static func createImagePicker() ->UIImagePickerController{
        let controller = UIImagePickerController()
        controller.allowsEditing = true
        controller.mediaTypes = ["public.image"]
        controller.sourceType = .camera
        return controller
    }
}

